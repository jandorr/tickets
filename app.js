//jshint esversion:6
import express from 'express';
import ejs from 'ejs';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import bcrypt from 'bcrypt';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import _ from 'lodash';

dotenv.config();

const saltRounds = 10;
const PORT = process.env.PORT;
const status = {
    open: 'Open',
    inprogress: 'In Progress',
    closed: 'Closed',
};

function get_badge(ticketStatus) {
    if (ticketStatus === status.open) return 'badge-warning';
    else if (ticketStatus === status.inprogress) return 'badge-success';
    else if (ticketStatus === status.closed) return 'badge-danger';
    else throw 'Not a valid status';
}

function get_priority_badge(ticketPriority) {
    if (ticketPriority === 'High') return 'badge-danger';
    else if (ticketPriority === 'Medium') return 'badge-warning';
    else if (ticketPriority === 'Low') return 'badge-light';
    else throw 'Not a valid status';
}

const app = express();
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
); // TODO: persistent storage
app.set('view engine', 'ejs');

// Database

mongoose.connect(
    'mongodb+srv://jan:' +
        process.env.DB_PASSWORD +
        '@cluster0.js8fa.mongodb.net/tickets?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true }
);

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
});
const User = new mongoose.model('User', userSchema);

const ticketSchema = new mongoose.Schema({
    title: String,
    titleTruncated: String,
    author: userSchema,
    assignee: userSchema,
    body: String,
    bodyTruncated: String,
    comments: [{ body: String, date: Date, user: userSchema }],
    date: { type: Date, default: Date.now },
    status: String,
    priority: String,
    mergeRequests: [String],
});
const Ticket = new mongoose.model('Ticket', ticketSchema);

// Middleware

function checkSessionUser(req, res, next) {
    if (req.session.user) {
        next();
    } else {
        res.redirect('/signin');
    }
}

// Routes

app.get('/search', checkSessionUser, async function (req, res) {
    try {
        let startTime = new Date();

        var tickets = await Ticket.find({
            $or: [
                { title: { $regex: req.query.q, $options: 'i' } },
                { 'author.name': req.query.q },
                { 'author.email': req.query.q },
                { 'assignee.name': req.query.q },
                { 'assignee.email': req.query.q },
                { body: { $regex: req.query.q, $options: 'i' } },
            ],
        });

        tickets.forEach((ticket) => {
            ticket.badge = get_badge(ticket.status);
            ticket.prioritybadge = get_priority_badge(ticket.priority);
        });

        let endTime = new Date();
        let timeDiff = endTime - startTime; //in ms
        timeDiff /= 1000;

        res.render('search', {
            title: `${tickets.length} results (${timeDiff.toFixed(2)} seconds)`,
            email: req.session.user.email,
            tickets: tickets,
        });
    } catch (e) {
        console.log(e.message);
        res.status(500).send('Server error');
    }
});

app.get('/', checkSessionUser, async function (req, res) {
    // Cookie
    // console.log('Cookies: ', req.cookies);
    // console.log('Session: ', req.session);
    // res.cookie('name', 'value', { maxAge: 10000 });
    // res.clearCookie('name');

    // Session
    if (req.session.page_views) {
        req.session.page_views++;
    } else {
        req.session.page_views = 1;
    }

    try {
        var tickets = await Ticket.find({ author: req.session.user });
    } catch (e) {
        console.log(err.message);
        res.status(500).send('Server error');
    }

    tickets.forEach((ticket) => {
        ticket.badge = get_badge(ticket.status);
        ticket.prioritybadge = get_priority_badge(ticket.priority);
    });

    res.render('home', {
        title: 'My Tickets',
        email: req.session.user.email,
        tickets: tickets,
    });
});

app.get('/all', checkSessionUser, async function (req, res) {
    // Cookie
    // console.log('Cookies: ', req.cookies);
    // console.log('Session: ', req.session);
    // res.cookie('name', 'value', { maxAge: 10000 });
    // res.clearCookie('name');

    // Session
    if (req.session.page_views) {
        req.session.page_views++;
    } else {
        req.session.page_views = 1;
    }

    try {
        var tickets = await Ticket.find({});
    } catch (e) {
        console.log(err.message);
        res.status(500).send('Server error');
    }

    tickets.forEach((ticket) => {
        ticket.badge = get_badge(ticket.status);
        ticket.prioritybadge = get_priority_badge(ticket.priority);
    });

    res.render('all', {
        title: 'All Tickets',
        email: req.session.user.email,
        tickets: tickets,
    });
});

app.get('/assigned', checkSessionUser, async function (req, res) {
    // Cookie
    // console.log('Cookies: ', req.cookies);
    // console.log('Session: ', req.session);
    // res.cookie('name', 'value', { maxAge: 10000 });
    // res.clearCookie('name');

    // Session
    if (req.session.page_views) {
        req.session.page_views++;
    } else {
        req.session.page_views = 1;
    }

    try {
        var tickets = await Ticket.find({ assignee: req.session.user });
    } catch (e) {
        console.log(err.message);
        res.status(500).send('Server error');
    }

    tickets.forEach((ticket) => {
        ticket.badge = get_badge(ticket.status);
        ticket.prioritybadge = get_priority_badge(ticket.priority);
    });

    res.render('assigned', {
        title: 'Assigned to me',
        email: req.session.user.email,
        tickets: tickets,
    });
});

app.get('/signin', function (req, res) {
    res.render('signin', { message: '' });
});

app.post('/signin', function (req, res) {
    User.findOne({ email: req.body.email }, function (err, foundUser) {
        if (err) {
            console.log(err);
        } else {
            if (foundUser) {
                bcrypt.compare(
                    req.body.password,
                    foundUser.password,
                    function (err, result) {
                        if (result === true) {
                            req.session.user = foundUser;
                            res.redirect('/');
                        } else {
                            res.render('signin', {
                                message:
                                    'Incorrect password. Please try again.',
                            });
                        }
                    }
                );
            } else {
                res.render('signin', {
                    message: 'User does not exist. Please sign-up.',
                });
            }
        }
    });
});

app.get('/signup', function (req, res) {
    res.render('signup', { message: '' });
});

app.post('/signup', function (req, res) {
    User.findOne({ email: req.body.email }, function (err, foundUser) {
        if (err) {
            console.log(err);
        } else {
            if (foundUser) {
                res.render('signup', {
                    message:
                        'User already exists! Login or choose another email address.',
                });
            } else {
                bcrypt.hash(
                    req.body.password,
                    saltRounds,
                    function (err, hash) {
                        const newUser = new User({
                            name: req.body.name,
                            email: req.body.email,
                            password: hash,
                        });

                        newUser.save(function (err) {
                            if (err) {
                                console.log(err);
                            } else {
                                req.session.user = newUser;
                                res.redirect('/');
                            }
                        });
                    }
                );
            }
        }
    });
});

app.get('/signout', function (req, res) {
    req.session.destroy(function () {
        console.log('user logged out.');
    });
    res.redirect('/signin');
});

app.get('/new', checkSessionUser, function (req, res) {
    res.render('new', { title: 'New Ticket', email: req.session.user.email });
});

app.post('/new', checkSessionUser, function (req, res) {
    const ticket = new Ticket({
        title: req.body.title,
        titleTruncated: _.truncate(req.body.title, { length: 30 }),
        author: req.session.user,
        body: req.body.body,
        bodyTruncated: _.truncate(req.body.body, { length: 100 }),
        status: status.open,
        priority: req.body.priority,
    });

    ticket.save(function (err) {
        if (err) {
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
});

app.get('/ticket/:ticketID', checkSessionUser, function (req, res) {
    Ticket.findOne({ _id: req.params.ticketID }, function (err, ticket) {
        if (err) return console.error(err);
        if (ticket) {
            req.session.ticketID = req.params.ticketID;
            res.render('ticket', {
                title: ticket.title,
                ticket: ticket,
                email: req.session.user.email,
            });
        }
    });
});

app.post('/ticket/:ticketID', checkSessionUser, function (req, res) {
    Ticket.findOne({ _id: req.params.ticketID }, async function (err, ticket) {
        if (err) return console.error(err);
        if (ticket) {
            if (req.body.assignee) {
                const newAssignee = await User.findOne({
                    email: req.body.assignee,
                });

                if (newAssignee) {
                    ticket.assignee = newAssignee;
                    res.json({ userNotFound: false });
                } else {
                    ticket.assignee = null;
                    res.json({ userNotFound: true });
                }
            }

            if (req.body.comment) {
                ticket.comments.push({
                    body: req.body.comment,
                    date: Date.now(),
                    user: req.session.user,
                });
                ticket.save();
                res.redirect(req.url);
                return;
            }

            ticket.priority = req.body.priority;
            ticket.status = req.body.status;
            ticket.save();
        }
    });
});

app.listen(PORT, function () {
    console.log('Server started on port ' + PORT);
});
